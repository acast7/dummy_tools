# tools
> This is a module for showing how nbdev works.


This is information about the `tools` library

## Install

1. set username, for example: `username=acast7`
2. run: `python -m pip install git+https://${username}@bitbucket.org/acast7/dummy_tools.git@v0.0.2`

## Overview
A Tour

- `adders` : module for adding numbers together 
- `tutorial` : examples on how to use tools here
